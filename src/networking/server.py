import asyncio

from .common import IO


async def create_server(ip, port):
    loop = asyncio.get_running_loop()
    server = await loop.create_server(lambda: IO(), ip, port)
    return server
